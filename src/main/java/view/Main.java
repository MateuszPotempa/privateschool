package view;

import controller.PrivateSchoolImpl;
import controller.dao.*;
import controller.wrappers.PrinterWrapperImpl;
import controller.wrappers.ScannerWrapperImpl;


public class Main {
    public static void main(String[] args) {

        PrivateSchoolImpl privateSchool=new PrivateSchoolImpl(new PrinterWrapperImpl(),new ScannerWrapperImpl(),new StudentToSubjectImpl(),
               new StudentToSubjectToGradeImpl(),new GradeDaoImpl(new StudentToSubjectToGradeImpl(),new StudentToSubjectImpl()),new StudentDaoImpl(),new SubjectDaoImpl());

       privateSchool.createClassRegister();

    }
}

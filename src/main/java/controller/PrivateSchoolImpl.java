package controller;

import controller.dao.*;
import controller.wrappers.PrinterWrapperImpl;
import controller.wrappers.ScannerWrapperImpl;
import model.models.*;

import java.util.List;
import java.util.stream.Collectors;


public class PrivateSchoolImpl implements PrivateSchool {

    private PrinterWrapperImpl printerWrapper;
    private ScannerWrapperImpl scannerWrapper;

    private GradeDaoImpl gradeDao;
    private StudentDaoImpl studentDao;
    private SubjectDaoImpl subjectDao;
    private StudentToSubjectImpl studentToSubjectDao;
    private StudentToSubjectToGradeImpl studentToSubjectToGradeDao;


    private long studentID;
    private long subjectID;

    public PrivateSchoolImpl(PrinterWrapperImpl printerWrapper, ScannerWrapperImpl scannerWrapper, StudentToSubjectImpl studentToSubject,
                             StudentToSubjectToGradeImpl studentToSubjectToGrade, GradeDaoImpl gradeDao,
                             StudentDaoImpl studentDao, SubjectDaoImpl subjectDao) {
        this.printerWrapper = printerWrapper;
        this.scannerWrapper = scannerWrapper;
        this.studentToSubjectDao = studentToSubject;
        this.studentToSubjectToGradeDao = studentToSubjectToGrade;
        this.gradeDao = gradeDao;
        this.studentDao = studentDao;
        this.subjectDao = subjectDao;
    }

    @Override
    public void createClass() {
        printerWrapper.print("Enter students NAME, SURNAME and AGE. If you want to finish operation please write EXIT");
        while (true) {
            String name = scannerWrapper.next();
            if (name.toLowerCase().equals("exit")) {
                break;
            }
            String surname = scannerWrapper.next();
            int age = scannerWrapper.nextInt();

            Student student = new Student(name, surname, age);
            studentDao.addStudent(student);
            createStudentToSubjectConnection(student);

            if (!studentDao.getStudents().isEmpty()) {
                printerWrapper.print("Please enter next student or EXIT: ");
            }
        }
    }

    @Override
    public void createStudentToSubjectConnection(Student student) {
        for (Subject subject : subjectDao.getSubjects()) {
            StudentToSubjectModel sts = new StudentToSubjectModel();
            sts.setSubjectID(subject.getSubjectID());
            sts.setStudentID(student.getStudentID());
            studentToSubjectDao.addSTS(sts);
        }
    }


    @Override
    public void createGrades(long studentID, long subjectID) {
        printerWrapper.print("Insert student's grades:");
        while (true) {
            String option = scannerWrapper.next();
            if (option.toLowerCase().equals("exit")) {
                break;
            } else {
                int mark = Integer.parseInt(option);
                if (mark < 1 || mark > 6) {
                    printerWrapper.print("Insert mark in range 1-6");
                    createGrades(studentID, subjectID);
                    break;
                }

                Grade grade = new Grade();
                grade.setGrade(mark);
                gradeDao.addGrade(grade);

                StudentToSubjectModel studentToSubjectChoice = studentToSubjectDao.getStsList().stream().filter(studentToSubjectModel ->
                        studentToSubjectModel.getStudentID() == studentID && studentToSubjectModel.getSubjectID() == subjectID).
                        findFirst().orElseThrow(NullPointerException::new);


                createStudentToSubjectToGradeConnection(grade, studentToSubjectChoice);


                if (!gradeDao.getGrades().isEmpty()) {
                    printerWrapper.print("Please enter next grade or EXIT: ");
                }
            }

        }
    }

    @Override
    public void createStudentToSubjectToGradeConnection(Grade grade, StudentToSubjectModel studentToSubjectModel) {

        StudentToSubjectToGradeModel studentToSubjectToGrade = new StudentToSubjectToGradeModel();
        studentToSubjectToGrade.setStudentToSubjectID(studentToSubjectModel.getstudentToSubjectID());
        studentToSubjectToGrade.setGradeID(grade.getGradeID());
        studentToSubjectToGradeDao.addSSG(studentToSubjectToGrade);

    }


    public void createClassRegister() {

        while (true) {
            printerWrapper.print("Choose required element:\n 1.Create list of students \n 2.Insert grades" +
                    "\n 3.Show list of students  \n 4.Show register \n 5.Show subject average \n 6.Improve grades");
            int option = scannerWrapper.nextInt();
            switch (option) {
                case 1:
                    createClass();
                    break;
                case 2:
                    insertStudentAndSubjectID();
                    createGrades(studentID, subjectID);
                    break;
                case 3:
                    studentDao.showStudents();
                    break;
                case 4:
                    insertStudentAndSubjectID();
                    showStudentSubjectGrades();
                    break;
                case 5:
                    insertStudentAndSubjectID();
                    double average=calculateAverage(studentID,subjectID);
                    printerWrapper.print(String.format("Grades average: %,.2f",average));
                    break;
                case 6:
                    insertStudentAndSubjectID();
                    printerWrapper.print("Please enter ID of a grade you would like to improve: ");
                    showStudentSubjectGrades();
                    long gradeID = scannerWrapper.nextLong();
                    printerWrapper.print("Please enter new grade:");
                    int newGrade = scannerWrapper.nextInt();

                    improveGrade(gradeID,newGrade);
                    break;
                default:
                    printerWrapper.print("Wrong parameter, please try again!");
                    createClassRegister();
            }
        }
    }

    private void insertStudentAndSubjectID() {
        printerWrapper.print("Please enter ID of a student:  ");
        studentDao.showStudents();
        studentID = scannerWrapper.nextLong();
        printerWrapper.print("Please enter ID of a subject: ");
        subjectDao.subjectShow();
        subjectID = scannerWrapper.nextLong();
    }

    private void improveGrade(long gradeID, int newGrade) {

        StudentToSubjectModel studToSubjModel = studentToSubjectDao.getStsList().stream().filter(studentToSubjectModel ->
                studentToSubjectModel.getStudentID() == studentID && studentToSubjectModel.getSubjectID() == subjectID).
                findFirst().orElseThrow(NullPointerException::new);


        StudentToSubjectToGradeModel studentToSubjectToGrade= studentToSubjectToGradeDao.getSSGList().stream().
                filter(studentToSubjectToGradeModel -> studentToSubjectToGradeModel.getStudentToSubjectID() == studToSubjModel.getstudentToSubjectID() &&
                        studentToSubjectToGradeModel.getGradeID() == gradeID).findFirst().orElseThrow(NullPointerException::new);

        Grade uptGrade=gradeDao.getGrades().stream().filter(grade->grade.getGradeID()==studentToSubjectToGrade.getGradeID()).findFirst().orElseThrow(NullPointerException::new);

        if(uptGrade.getGradeSetter()>1){
            printerWrapper.print("Sorry, but grade can be improved just once!");
        }
        else {
            uptGrade.setGrade(newGrade);
        }
    }
        public void showSubjects() {
            StudentToSubjectModel studentToSubjectModel = studentToSubjectDao.getStsList().stream().filter(sts -> sts.getStudentID() == studentID).findFirst().
                    orElseThrow(NullPointerException::new);

            for (int i = 0; i < subjectDao.getSubjects().size(); i++) {
                System.out.println(subjectDao.getSubjects().get(i));
            }
        }

        public void showStudentSubjectGrades() {

            StudentToSubjectModel studentToSubjectChoice = studentToSubjectDao.getStsList().stream().filter(studentToSubjectModel ->
                    studentToSubjectModel.getStudentID() == studentID && studentToSubjectModel.getSubjectID() == subjectID).
                    findFirst().orElseThrow(NullPointerException::new);

            List<StudentToSubjectToGradeModel> gradeCollect = studentToSubjectToGradeDao.getSSGList().stream().filter(studentToSubjectToGradeModel ->
                    studentToSubjectToGradeModel.getStudentToSubjectID() == studentToSubjectChoice.getstudentToSubjectID()).collect(Collectors.toList());

            for (int i = 0; i < gradeCollect.size(); i++) {

                int iterator = i;
                gradeDao.getGrades().stream().filter(grade -> grade.getGradeID() == gradeCollect.get(iterator).getGradeID()).forEach(System.out::println);
            }
        }

    public double calculateAverage(long studentID, long subjectID) {

        StudentToSubjectModel studentToSubjectChoice = studentToSubjectDao.getStsList().stream().filter(studentToSubjectModel ->
                studentToSubjectModel.getStudentID() == studentID && studentToSubjectModel.getSubjectID() == subjectID).
                findFirst().orElseThrow(NullPointerException::new);

        List<StudentToSubjectToGradeModel> gradeCollect = studentToSubjectToGradeDao.getSSGList().stream().filter(studentToSubjectToGradeModel ->
                studentToSubjectToGradeModel.getStudentToSubjectID() == studentToSubjectChoice.getstudentToSubjectID()).collect(Collectors.toList());

        List<Grade>listOfGrades=gradeCollect.stream().map(studentToSubjectToGradeModel ->
                gradeDao.getGrades().stream().filter(grade -> grade.getGradeID()==studentToSubjectToGradeModel.getGradeID())).
                findFirst().orElseThrow().collect(Collectors.toList());


        return listOfGrades.stream().mapToDouble(Grade::getGrade).average().orElse(0.0);
    }

    }


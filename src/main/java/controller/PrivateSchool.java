package controller;


import model.models.Grade;
import model.models.Student;
import model.models.StudentToSubjectModel;

public interface PrivateSchool {

    void createClass();
    void createGrades(long studentID, long subjectID);
    void createStudentToSubjectConnection(Student student);
    void createStudentToSubjectToGradeConnection(Grade grade,StudentToSubjectModel studentToSubjectModel);



}

package controller.wrappers;

public interface ScannerWrapper {
    int nextInt();
    long nextLong();
    String next();
}

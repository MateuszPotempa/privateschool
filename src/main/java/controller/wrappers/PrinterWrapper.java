package controller.wrappers;

public interface PrinterWrapper {

    void print(String text);
}

package controller.wrappers;

import java.util.Scanner;

public class ScannerWrapperImpl implements ScannerWrapper {

    @Override
    public int nextInt() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public long nextLong() {
          return  new Scanner(System.in).nextLong();
    }

    @Override
    public String next() {
        return new Scanner(System.in).next();
    }

}

package controller.dao;

import model.models.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDaoImpl implements StudentDao {

    private List<Student> listOfStudents = new ArrayList<>();


    @Override
    public void addStudent(Student student){
        if(listOfStudents.isEmpty()){
            student.setStudentID(1);
        }
        else{
            student.setStudentID(listOfStudents.get(listOfStudents.size()-1).getStudentID()+1);
        }
        listOfStudents.add(student);
    }

    @Override
    public void removeStudent(long removedID) {
        listOfStudents=listOfStudents.stream().filter(student -> student.getStudentID()!=removedID).collect(Collectors.toList());

    }

    @Override
    public void updateStudent(Student updatedStudent) {
        int index=listOfStudents.indexOf(listOfStudents.stream().filter(student -> student.getStudentID()==updatedStudent.getStudentID()).findFirst().orElseThrow(NullPointerException::new));
        listOfStudents.add(index,updatedStudent);
    }

    @Override
    public List<Student> getStudents() {
        return listOfStudents;
    }

    @Override
    public void showStudents() {
        listOfStudents.forEach(System.out::println);
    }


}



package controller.dao;

import model.models.StudentToSubjectToGradeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentToSubjectToGradeImpl implements StudentToSubjectToGrade {

    private List<StudentToSubjectToGradeModel> listSSG=new ArrayList<>();

    @Override
    public void addSSG(StudentToSubjectToGradeModel ssg) {
        if(listSSG.isEmpty()){
            ssg.setStudentToSubjectToGradeID(1);
        }
        else{
            ssg.setStudentToSubjectToGradeID(listSSG.get(listSSG.size()-1).getStudentToSubjectToGradeID()+1);
        }
        listSSG.add(ssg);
    }

    @Override
    public void removeSSG(long removedID) {
        listSSG=listSSG.stream().filter(ssg -> ssg.getStudentToSubjectID()!=removedID).collect(Collectors.toList());

    }

    @Override
    public void updateSSG(StudentToSubjectToGradeModel updatedSsg) {
        int index=listSSG.indexOf(listSSG.stream().filter(ssg -> ssg.getStudentToSubjectID()==updatedSsg.getStudentToSubjectID()).findFirst().orElseThrow(NullPointerException::new));
        listSSG.add(index,updatedSsg);
    }

    @Override
    public List<StudentToSubjectToGradeModel> getSSGList() {
        return listSSG;
    }
}


package controller.dao;

import model.models.Subject;

import java.util.List;

public interface SubjectDao {

    List<Subject> getSubjects();
    void subjectShow();



}

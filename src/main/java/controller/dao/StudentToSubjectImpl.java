package controller.dao;

import model.models.StudentToSubjectModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentToSubjectImpl implements StudentToSubject {

    private List<StudentToSubjectModel> listSTS=new ArrayList<>();

    @Override
    public void addSTS(StudentToSubjectModel sts) {
            if(listSTS.isEmpty()){
                sts.setstudentToSubjectID(1);
            }
            else{
                sts.setstudentToSubjectID(listSTS.get(listSTS.size()-1).getstudentToSubjectID()+1);
            }
            listSTS.add(sts);
    }

    @Override
    public void removeSTS(long removedID) {
        listSTS=listSTS.stream().filter(sts -> sts.getstudentToSubjectID()!=removedID).collect(Collectors.toList());

    }

    @Override
    public void updateSTS(StudentToSubjectModel updatedSts) {
        int index=listSTS.indexOf(listSTS.stream().filter(sts -> sts.getstudentToSubjectID()==updatedSts.getstudentToSubjectID()).findFirst().orElseThrow(NullPointerException::new));
        listSTS.add(index,updatedSts);
    }

    @Override
    public List<StudentToSubjectModel> getStsList() {
        return listSTS;
    }
}

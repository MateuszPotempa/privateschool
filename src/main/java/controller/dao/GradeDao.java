package controller.dao;

import model.models.Grade;
import model.models.Student;
import model.models.Subject;
import org.w3c.dom.ls.LSOutput;

import java.util.List;

public interface GradeDao {
    List<Grade> getGrades();

    void addGrade (Grade addGrade);
    void removeGrade(long iD);
    void updateGrade(Grade updatedGrade);




}

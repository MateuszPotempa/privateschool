package controller.dao;

import model.models.StudentToSubjectModel;

import java.util.List;

public interface StudentToSubject {

    List<StudentToSubjectModel> getStsList();

    void addSTS (StudentToSubjectModel addStudent);
    void removeSTS (long removedID);
    void updateSTS (StudentToSubjectModel student);
}

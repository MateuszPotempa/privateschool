package controller.dao;

import model.models.Student;
import model.models.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectDaoImpl implements SubjectDao {

    private List<Subject> listOfSubjects=new ArrayList<>(List.of(
            new Subject("English").setSubjectID(1),
            new Subject("Math").setSubjectID(2),
            new Subject("Physics").setSubjectID(3),
            new Subject("Geology").setSubjectID(4),
            new Subject("Polish").setSubjectID(5),
            new Subject("History").setSubjectID(6)));


    @Override
    public List<Subject> getSubjects() {
        return listOfSubjects;
    }

    @Override
    public void subjectShow() {
        listOfSubjects.forEach(System.out::println);
    }


}

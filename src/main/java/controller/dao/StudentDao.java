package controller.dao;

import model.models.Student;

import java.util.List;

public interface StudentDao {

    List<Student> getStudents();

    void showStudents();
    void addStudent (Student addStudent);
    void removeStudent (long removedID);
    void updateStudent (Student student);


}

package controller.dao;

import model.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GradeDaoImpl implements GradeDao {

private List<Grade> listOfGrades=new ArrayList<>();
private StudentToSubjectToGradeImpl studentToSubjectToGrade;
private StudentToSubjectImpl studentToSubject;

    public GradeDaoImpl(StudentToSubjectToGradeImpl studentToSubjectToGrade, StudentToSubjectImpl studentToSubject) {
        this.studentToSubjectToGrade = studentToSubjectToGrade;
        this.studentToSubject = studentToSubject;
    }

    @Override
    public void addGrade(Grade grade) {
        if(listOfGrades.isEmpty()){
        grade.setGradeID(1);
    }
        else{
        grade.setGradeID(listOfGrades.get(listOfGrades.size()-1).getGradeID()+1);
    }
        listOfGrades.add(grade);
    }

    @Override
    public void removeGrade(long removedID) {
        listOfGrades=listOfGrades.stream().filter(grade -> grade.getGradeID()!=removedID).collect(Collectors.toList());
    }

    @Override
    public void updateGrade(Grade updatedGrade) {

        int index=listOfGrades.indexOf(listOfGrades.stream().filter(grade -> grade.getGradeID()==updatedGrade.getGradeID())
                .findFirst().orElseThrow(NullPointerException::new));
        //listOfGrades.add(index,updatedGrade);
    }



    @Override
    public List<Grade> getGrades() {
        return listOfGrades;
    }
}

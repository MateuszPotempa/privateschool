package controller.dao;

import model.models.StudentToSubjectToGradeModel;

import java.util.List;

public interface StudentToSubjectToGrade {

    List<StudentToSubjectToGradeModel> getSSGList();

    void addSSG (StudentToSubjectToGradeModel addStudent);
    void removeSSG (long removedID);
    void updateSSG (StudentToSubjectToGradeModel student);
}

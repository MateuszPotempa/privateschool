package model.models;

public class StudentToSubjectToGradeModel {

    private long studentToSubjectToGrade=0L;
    private long studentToSubjectID=0L;
    private long gradeID=0L;

    public long getStudentToSubjectToGradeID() {
        return studentToSubjectToGrade;
    }

    public void setStudentToSubjectToGradeID(long studentToSubjectToGrade) {
        this.studentToSubjectToGrade = studentToSubjectToGrade;
    }

    public long getStudentToSubjectID() {
        return studentToSubjectID;
    }

    public void setStudentToSubjectID(long studentToSubjectID) {
        this.studentToSubjectID = studentToSubjectID;
    }

    public long getGradeID() {
        return gradeID;
    }
    public void setGradeID(long gradeID) {
        this.gradeID = gradeID;
    }
}

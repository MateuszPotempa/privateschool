package model.models;

public class Subject {

    private long subjectID=0L;
    private String subjectName;

    public Subject(String subjectName) {
        this.subjectName = subjectName;
    }

    public long getSubjectID() {
        return subjectID;
    }

    public Subject setSubjectID(long subjectID) {
        this.subjectID = subjectID;
        return this;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return "Subject ID "+subjectID+
                " " + subjectName;
    }
}

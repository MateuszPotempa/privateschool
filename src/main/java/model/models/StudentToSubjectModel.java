package model.models;

public class StudentToSubjectModel {

    private long studentToSubjectID=0L;
    private long studentID=0L;
    private long subjectID=0L;

    public long getstudentToSubjectID() {
        return studentToSubjectID;
    }

    public void setstudentToSubjectID(long studentToSubjectID) {
        this.studentToSubjectID = studentToSubjectID;
    }

    public long getStudentID() {
        return studentID;
    }

    public void setStudentID(long studentID) {
        this.studentID = studentID;
    }

    public long getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(long subjectID) {
        this.subjectID = subjectID;
    }

}

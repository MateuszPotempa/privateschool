package model.models;

public class Grade {

    private long gradeID=0L;
    private int grade;
    private int gradeSetter;

    public Grade() {
    }

    public long getGradeID() {
        return gradeID;
    }

    public void setGradeID(long gradeID) {
        this.gradeID = gradeID;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        gradeSetter++;
        if(gradeSetter>2) {
            return;}
        this.grade = grade;
    }

    public int getGradeSetter() {
        return gradeSetter;
    }

    @Override
    public String toString() {
        return "GradeID " + gradeID +
                " grade=" + grade;
    }
}
